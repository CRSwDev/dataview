# BD(TM) DataView

This repository contains BD(TM) DataView software for visualization and exploratory analysis of output files generated from bioinformatics analysis Both Mac and PC version are available. 

Download [BD DataView v1.2.2 (MAC)](https://s3.amazonaws.com/dataview/DataView_v122_MAC.zip)

Download [BD DataView v1.2.2 (PC)](https://s3.amazonaws.com/dataview/DataView_v122_PC.zip)

## Downloading Demo datasets
Demo datasets can be downloaded [here](http://bd-rhapsody-public.s3-website-us-east-1.amazonaws.com/DataView-Demo-Data-Sets/).

## Prerequisites for Running DataView v1.2.2

Verify that version 9.3 (R2017b) of the MATLAB Runtime is installed.   

If the MATLAB Runtime is not installed, download and install the Macintosh or Windows version of the MATLAB Runtime for R2017b 
from the following link on the MathWorks website:

   [http://www.mathworks.com/products/compiler/mcr/index.html](http://www.mathworks.com/products/compiler/mcr/index.html)
   
For more information about the MATLAB Runtime and the MATLAB Runtime installer, see 
Package and Distribute in the MATLAB Compiler documentation  
in the MathWorks Documentation Center.    

NOTE: You will need administrator rights to run the MATLAB Runtime installer. 

## v1.2.2 Release Notes - July 31, 2018
For detailed description of new features, please refer to the Data View chapter of [BD Rhapsody(TM) genomics bioinformatics handbook user guide](https://www.bd.com/documents/guides/user-guides/GMX_BD-Rhapsody-genomics-informatics_UG_EN.pdf).

New Features:

	•	Added features to analyze AbSeq data:
		•	Automatically detect AbSeq markers in loaded data table and generate a list of AbSeq markers in the preloaded gene set list for 'Filter data table' use
		•	New Gene A vs. B feature which allows user to compare expression of two gene markers side-by side
		•	Overlapping histograms for single- and multi-gene marker plots grouped by annotation
	
	•	Updated UI design and organization
	
	•	Added option to combine two or more data tables

	•	Added option to allow differential expression analysis for > 1500 genes

	•	Added option to save plots in different formats (pdf, png, svg)

	•	Added option to order annotated groups in scatter plots, box plots, and heat maps

	•	Added option to display median marker expression of each annotated group in box plot

	•	Added option to order genes based on expression in heat map

	•	Updated filter/search gene name function to allow search for partial name match 
    

Bug Fixes:

	•	Fixed error related to loading multiple data tables in .mtx format

	•	Fixed legend ordering for group A vs. B in differential expression scatter plot

	•	Fixed log10 relative expression calculation in Heat Map