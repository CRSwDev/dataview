# BD(TM) DataView

This repository contains BD(TM) DataView software for visualization and exploratory analysis of output files generated from bioinformatics analysis Both Mac and PC version are available. 

## Downloading Demo datasets
Demo datasets can be downloaded from http://bd-rhapsody-public.s3-website-us-east-1.amazonaws.com/DataView-Demo-Data-Sets/

## Prerequisites for Running DataView v1.1.0

Verify that version 9.3 (R2017b) of the MATLAB Runtime is installed.   

If the MATLAB Runtime is not installed, download and install the Macintosh or Windows version of the MATLAB Runtime for R2017b 
from the following link on the MathWorks website:

    http://www.mathworks.com/products/compiler/mcr/index.html
   
For more information about the MATLAB Runtime and the MATLAB Runtime installer, see 
Package and Distribute in the MATLAB Compiler documentation  
in the MathWorks Documentation Center.    

NOTE: You will need administrator rights to run the MATLAB Runtime installer. 

## v1.1.0 Release Notes - Dec 19, 2017
MATLAB Version Update:

	•	MATLAB Runtime v9.3 (R2017b) is needed to run DataView v1.1.0. This update is fixes incompatibility issue with running DataView v1.0.6 on macOS Sierra 10.12. 

New Features:

	•	Added more color options for plots
    
	•	Added option to highlight selected annotated groups in scatter plots
    
	•	Added option to filter data table by cells based on gene expression
    
	•	Added calculation on fold changes and mean gene expression and export of these values in ‘Differential expression analysis’
    
	•	Added 2 new preloaded gene sets ‘Human X Chromosome’ and ‘Human Y Chromosome’  for ‘Filter data table’ use. 
    
	•	Added option to modify data table names and annotation list names

Bug Fixes:

	•	Fixed error related to loading data tables with no cell identifier